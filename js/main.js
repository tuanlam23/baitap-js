//Kiểm tra các trường tên, mail, số điện thoại
function validateName(username){
    re = /^[a-zA-Z]/;
    return re.test(username);

}
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
function validateNumber(phone){
    var re = /^[0-9]/;
    return re.test(phone);
}
function validateCompany(company){
    re = /^[a-zA-Z]/;
    return re.test(company);
}

function validateForm(){
        var flag = true ;
        var username = $('#username').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        var company = $('#company').val();
        var ghichu = $('ghichu').val();
//Bước 2 : Validate dữ liệu

        if (!validateName(username)) {
            $('#z-username').text('Chưa nhập tên');
            flag= false;
        }
        else {
            $('#z-username').text('');
        }

        if (!validateEmail(email)) {
            $('#z-email').text('Chưa nhập Email');
            flag= false;
        }
        else {
            $('#z-email').text('');
        }

        if (!validateNumber(phone)) {
            $('#z-phone').text('Chưa nhập Số điện thoại');
            flag= false;
        }
        else {
            $('#z-phone').text('');
        }
        if (company== "") {
            $('#z-company').text('Chưa nhập Công ty');
            flag= false;
        }
        else {
            $('#z-company').text('');
        }
        return flag;
}

$(document).ready(function(){
    $("#form_submit").on("click",function(){
        var flag;
        flag = validateForm();
        if(!flag) {
            $("#submit").attr('disabled', 'disabled'); 
        }else {
            $("#submit").removeAttr('disabled');
        }
    });   

    $("#submit").on("click", function(){
        var data = $('#form_submit').serialize(); 
        $.ajax({
            url: "https://reqres.in/api/users",
            method: "POST",
            data: data,
            success : function(response){
                alert("Them user thanh cong !");
                console.log(response);
            }
        });
        return false;
    });
});